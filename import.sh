#!/bin/bash -e

# Extra psql options and connection parameters
# The procedure runs much faster if the connection with the server is fast
params=$@

function run_sql() {
    psql -v ON_ERROR_STOP=1 -1 $params
}

function load_sql() {
    for filename in "$@"
    do
        echo "\echo Loading $(basename $filename)..."
        echo "\i $filename"
    done | run_sql
}

function run_template() {
    set -a
    template=$1
    schema=$2
    table=$3
    external_schema=$4
    external_table=$5
    shift 5
    for filename in "$@"
    do
        IFS='_' read -r -a data <<< "$(basename $filename .csv)"
        pos=0
        [ "${data[pos]}" == "norway" ] && ((pos++)) || true
        [ "${data[pos]}" == "north" ] && ((pos++)) || true
        [ "${data[pos]}" == "south" ] && ((pos++)) || true
        year="$(echo ${data[pos]} | cut -b-4)" # get year from period
        ((pos+=2))
        activity_type="${data[$((pos++))]}"
        ((pos++)) # edge/rollup
        if [ "${data[pos]}" == "month" ]
        then
            ((pos++))
            year="${data[$((pos++))]}"
            month="${data[$((pos++))]}"
        else
            month="12"
        fi
        echo "\echo === Importing $(basename $filename) into \"$schema\".$table..."
        envsubst < "$template"
    done | run_sql
    set +a
}

function create_schemas() {
    for schema in "$@"
    do
        echo "create schema if not exists $schema;"
    done | run_sql
}

function move_to_schema() {
    table=$1
    schema=$2
    echo "alter table $table set schema $schema;" | run_sql
}

function add_pk() {
    table=$1
    column=$2
    echo "alter table $table add primary key ($column);" | run_sql
}

shopt -s globstar

# Initialize PostGIS
echo "create extension if not exists postgis;" | run_sql

# Create schema for geometries
geometries_schema="strava_geometries"
create_schemas $geometries_schema

# Import edges
osm=**/*_osm_*.sql
osm_table=$(basename $osm .sql)
load_sql $osm
move_to_schema $osm_table $geometries_schema

# Import polygons
polygons=**/*_od_polygons_hex_*.sql
polygons_table=$(basename $polygons .sql)
load_sql $polygons
move_to_schema $polygons_table $geometries_schema

# Add missing primary keys
# This does not seem to work when running it in the same transaction together with load_sql
add_pk $polygons_table "id"

# Import Strava data
TMPL_DIR="$(dirname ${BASH_SOURCE[0]})/templates"

schema="strava"
create_schemas $schema

# key: template/hourly data file parameter
# value: path regex
# template name based on the key
declare -A paths=(
    ["edges"]="*_edges/Edges"
    ["od"]="*_od/OD"
)
declare -A geometry_tables=(
    ["edges"]="$osm_table"
    ["od"]="$polygons_table"
)

base_path="20*/"
for type in "${!paths[@]}"
do
    path="${paths[$type]}"
    geometry_table="${geometry_tables[$type]}"
    # Fix edge case
    file_param="$type"
    [ "$file_param" == "edges" ] && file_param="edge_alignment"
    # Import hourly
    run_template "$TMPL_DIR/${type}_hourly.tmpl" $schema "${type}_hourly" $geometries_schema $geometry_table $base_path/$path/*_${file_param}_hourly.csv

    for suffix in "_total" "_weekday" "_weekend"
    do
      # Import rollups
      run_template "$TMPL_DIR/${type}_rollup.tmpl" $schema "${type}_rollup_year$suffix" $geometries_schema $geometry_table $base_path/$path/*_rollup_total${suffix/_total/}.csv
      # Import rollups (season)
      run_template "$TMPL_DIR/${type}_rollup.tmpl" $schema "${type}_rollup_season$suffix" $geometries_schema $geometry_table $base_path/$path/*_rollup_season_0$suffix.csv
      # Import rollups (months)
      run_template "$TMPL_DIR/${type}_rollup.tmpl" $schema "${type}_rollup_month$suffix" $geometries_schema $geometry_table $base_path/$path/*_rollup_month_????_{1..12}$suffix.csv
    done
done
