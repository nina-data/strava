--drop schema strava_views cascade;
create schema strava_views;

select 'create view strava_views.' || table_name || '_with_geometry as' ||
         ' select "table".*, osm.geom' ||
         ' from ' || quote_ident(table_schema) || '.' || table_name || ' "table"' ||
         ' join strava_geometries.norway_osm_20191217 osm' ||
         '   on osm.id = "table".edge_id'
  from information_schema.tables
 where table_schema = 'strava' and table_name like 'edges_%';
\gexec

select 'create view strava_views.' || table_name || '_with_geometry as'  ||
         ' select "table".*, src.geom as src_geom, dst.geom dst_geom' ||
         ' from ' || quote_ident(table_schema) || '.' || table_name || ' "table"' ||
         ' join strava_geometries.norway_od_polygons_hex_20191217_polygons src' ||
         '   on src.id = "table".origin' ||
         ' join strava_geometries.norway_od_polygons_hex_20191217_polygons dst' ||
         '   on dst.id = "table".destination'
  from information_schema.tables
 where table_schema = 'strava' and table_name like 'od_rollup_%';
\gexec

select 'create view strava_views.' || table_name || '_with_geometry as' ||
         ' select "table".*, src.geom as src_geom, dst.geom dst_geom' ||
         ' from ' || quote_ident(table_schema) || '.' || table_name || ' "table"' ||
         ' join strava_geometries.norway_od_polygons_hex_20191217_polygons src' ||
         '   on src.id = "table".polygon_id' ||
         ' join strava_geometries.norway_od_polygons_hex_20191217_polygons dst' ||
         '   on dst.id = "table".dest_polygon_id'
  from information_schema.tables
 where table_schema = 'strava' and table_name like 'od_hourly';
\gexec


-- Strava Metro
create schema if not exists "Strava_Metro_views";

select 'create or replace view "Strava_Metro_views"."all_edges_monthly_ride" as ' ||
       'select concat(t.edge_uid, ''-'', t.month) as pk, * from (' ||
       'select * from ' || string_agg(t.name, ' union all select * from ') || ') as t' as names
  from (
        select quote_ident(table_schema) || '.' || quote_ident(table_name) as name
          from information_schema.tables
         where table_schema = 'Strava_Metro' and table_name like 'all_edges_monthly_%_ride'
         order by table_name
       ) t
 group by true;
\gexec

select 'create or replace view "Strava_Metro_views"."all_edges_monthly_ped" as ' ||
       'select concat(t.edge_uid, ''-'', t.month) as pk, * from (' ||
       'select * from ' || string_agg(t.name, ' union all select * from ') || ') as t' as names
  from (
        select quote_ident(table_schema) || '.' || quote_ident(table_name) as name
          from information_schema.tables
         where table_schema = 'Strava_Metro' and table_name like 'all_edges_monthly_%_ped'
               and table_name not like '%2021%'
         order by table_name
       ) t
 group by true;
\gexec
