select 'grant usage on schema ' ||
         string_agg(quote_ident(schema_name), ', ') || ' to ' || quote_ident(:'user')
 from information_schema.schemata
where schema_name ilike 'strava%';
\gexec
select 'grant select on all tables in schema ' ||
         string_agg(quote_ident(schema_name), ', ') || ' to ' || quote_ident(:'user')
 from information_schema.schemata
where schema_name ilike 'strava%';
\gexec
