# Project status

This project is dead, since Strava provides these data only via Strava Metro exports.

# Import data

```
cd ${strava_data_folder}
bash ${strava_import_folder}/import.sh ${postgres_connection_str}
```


# Missing data

If the directory structure differs (due to different ways to decompress the archives), some CSV will not be imported. Check what is missing by comparing the files produced by those commands:

```
find . -name '*.csv' -path "./20*" -exec basename "{}" \; | sort > find.txt
./import.sh $postgres_connection_str | grep "^===" | awk '{ print $3 }' | sort > imported.txt
```

# Give access to users

List users:

```
psql ${postgres_connection_str} -c '\du'
```

```
psql ${postgres_connection_str} -1 -f permissions.sql -v user="$username"
```

# Create views

```
psql ${postgres_connection_str} -1 -f views.sql
```
