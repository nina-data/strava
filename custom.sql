create schema strava_custom;

select 'create materialized view strava_custom.vinje_area_' || table_name || ' as' ||
         ' select row_number() over (), "table".*, navn' ||
         '   from ' || quote_ident(table_schema) || '.' || table_name || ' "table"' ||
         '   join "miljodata"."kommuner_2020_4326" kommuner' ||
         '     on ST_Intersects(kommuner.geom, "table".geom)' ||
         '  where navn in (''Seljord'', ''Tinn'', ''Hjartdal'', ''Vinje'', ''Tokke'', ''Kviteseid'', ''Notodden'')'
  from information_schema.views
 where table_schema = 'strava_views' and table_name like 'edges_%';
\gexec

select 'create materialized view strava_custom.lillehammer_area_' || table_name || ' as' ||
         ' select row_number() over (), "table".*, navn' ||
         '   from ' || quote_ident(table_schema) || '.' || table_name || ' "table"' ||
         '   join "miljodata"."kommuner_2020_4326" kommuner' ||
         '     on ST_Intersects(kommuner.geom, "table".geom)' ||
         '  where navn in (''Lillehammer'', ''Ringebu'', ''Stor-Elvdal'', ''Åmot'', ''Løten'', ''Hamar'', ''Øyer'', ''Ringsaker'')'
  from information_schema.views
 where table_schema = 'strava_views' and table_name like 'edges_%';
\gexec



select 'create materialized view strava_custom.vinje_area_' || table_name || ' as'  ||
         ' select row_number() over (), "table".*, kommuner1.navn as src_navn, kommuner2.navn as dst_navn' ||
         '   from ' || quote_ident(table_schema) || '.' || table_name || ' "table"' ||
         '   join "miljodata"."kommuner_2020_4326" kommuner1' ||
         '     on ST_Intersects(kommuner1.geom, "table".src_geom)' ||
         '   join "miljodata"."kommuner_2020_4326" kommuner2' ||
         '     on ST_Intersects(kommuner2.geom, "table".dst_geom)' ||
         '  where kommuner1.navn in (''Seljord'', ''Tinn'', ''Hjartdal'', ''Vinje'', ''Tokke'', ''Kviteseid'', ''Notodden'')' ||
         '     or kommuner2.navn in (''Seljord'', ''Tinn'', ''Hjartdal'', ''Vinje'', ''Tokke'', ''Kviteseid'', ''Notodden'')'
  from information_schema.tables
 where table_schema = 'strava_views' and table_name like 'od_%';
\gexec

select 'create materialized view strava_custom.lillehammer_area_' || table_name || ' as'  ||
         ' select row_number() over (), "table".*, kommuner1.navn as src_navn, kommuner2.navn as dst_navn' ||
         '   from ' || quote_ident(table_schema) || '.' || table_name || ' "table"' ||
         '   join "miljodata"."kommuner_2020_4326" kommuner1' ||
         '     on ST_Intersects(kommuner1.geom, "table".src_geom)' ||
         '   join "miljodata"."kommuner_2020_4326" kommuner2' ||
         '     on ST_Intersects(kommuner2.geom, "table".dst_geom)' ||
         '  where kommuner1.navn in (''Lillehammer'', ''Ringebu'', ''Stor-Elvdal'', ''Åmot'', ''Løten'', ''Hamar'', ''Øyer'', ''Ringsaker'')' ||
         '     or kommuner2.navn in (''Lillehammer'', ''Ringebu'', ''Stor-Elvdal'', ''Åmot'', ''Løten'', ''Hamar'', ''Øyer'', ''Ringsaker'')'
  from information_schema.tables
 where table_schema = 'strava_views' and table_name like 'od_%';
\gexec

select 'create materialized view strava_custom.Norefjell_area_' || table_name || ' as' ||
         ' select row_number() over (), "table".*' ||
         '   from ' || quote_ident(table_schema) || '.' || table_name || ' "table"' ||
         '   join "miljodata"."Norefjell_AOI" area' ||
         '     on ST_Intersects(st_transform(area.geom, 4326), "table".geom)'
  from information_schema.views
 where table_schema = 'strava_views' and table_name like 'edges_%';
\gexec

select 'create materialized view strava_custom.Glitterheim_area_' || table_name || ' as' ||
         ' select row_number() over (), "table".*' ||
         '   from ' || quote_ident(table_schema) || '.' || table_name || ' "table"' ||
         '   join "miljodata"."Glitterheim" area' ||
         '     on ST_Intersects(st_transform(area.geom, 4326), "table".geom)'
  from information_schema.views
 where table_schema = 'strava_views' and table_name like 'edges_%';
\gexec

-- QGIS needs a primary key, otherwise it use a random (?) integer column
-- Creating a unique index fixes the problem
-- References:
-- - https://issues.qgis.org/issues/9803
-- - https://github.com/qgis/QGIS/issues/32523
select 'create unique index ON ' || quote_ident(schemaname) || '.' || matviewname || ' (row_number)'
  from pg_catalog.pg_matviews
 where schemaname = 'strava_custom';
\gexec
-- Materialized views are not shown in information_schema:
-- - https://www.postgresql.org/message-id/flat/CAH7T-ao6ece1mgCsCvsE04W59ZZJP9gGXVK97wkUzRV5gsDqQQ%40mail.gmail.com
-- - https://dba.stackexchange.com/a/54101
