#!/bin/bash

for username in bram.van.moorter frank.hanssen vegard.brathen neri.thorsen frafra alexander.venter monica.ruano megan.nowell erik.stange postgjest
do
    psql $@ -1 -P pager -f permissions.sql -v user="$username"
done
